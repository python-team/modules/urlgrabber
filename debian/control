Source: urlgrabber
Section: python
Priority: optional
Maintainer: Kevin Coyner <kcoyner@debian.org>
Uploaders:
 Debian Python Team <team+python@tracker.debian.org>,
Build-Depends:
 debhelper-compat (= 9),
 dh-python,
 python-all,
 python-setuptools,
 python-six,
 python-pycurl,
 python3-all,
 python3-setuptools,
 python3-six,
 python3-pycurl,
Standards-Version: 4.1.1
Homepage: http://urlgrabber.baseurl.org/
Vcs-Git: https://salsa.debian.org/python-team/packages/urlgrabber.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/urlgrabber

Package: python-urlgrabber
Architecture: all
Depends:
 python-pycurl,
 python-six,
 ${misc:Depends},
 ${python:Depends},
Description: high-level URL transfer library - Python 2.x
 urlgrabber dramatically simplifies the fetching of files. It is designed to
 be used in programs that need common (but not necessarily simple)
 url-fetching features. This package provides both a binary and a module, both
 of the name urlgrabber.
 .
 It supports identical behavior for http://, ftp:// and file:/// URIs. It
 provides HTTP keepalive, byte ranges, regets, progress meters, throttling,
 retries, access to authenticated http/ftp servers, and proxies. Additionally
 it has the ability to treat a list of mirrors as a single source and to
 automatically switch mirrors if there is a failure.
 .
 Python 2 version (deprecated, soon removed).

Package: python3-urlgrabber
Architecture: all
Depends:
 python3-pycurl,
 python3-six,
 ${misc:Depends},
 ${python3:Depends},
Description: high-level URL transfer library
 urlgrabber dramatically simplifies the fetching of files. It is designed to
 be used in programs that need common (but not necessarily simple)
 url-fetching features. This package provides both a binary and a module, both
 of the name urlgrabber.
 .
 It supports identical behavior for http://, ftp:// and file:/// URIs. It
 provides HTTP keepalive, byte ranges, regets, progress meters, throttling,
 retries, access to authenticated http/ftp servers, and proxies. Additionally
 it has the ability to treat a list of mirrors as a single source and to
 automatically switch mirrors if there is a failure.
